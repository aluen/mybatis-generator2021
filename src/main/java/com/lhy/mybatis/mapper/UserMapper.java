package com.lhy.mybatis.mapper;

import com.lhy.mybatis.entities.User;
import tk.mybatis.mapper.common.Mapper;

public interface UserMapper extends Mapper<User> {
}